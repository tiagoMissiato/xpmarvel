package com.tiagomissiato.xpmarvel.characterdetail

import com.tiagomissiato.xpmarvel.characterlist.CharacterListContract
import com.tiagomissiato.xpmarvel.model.*
import com.tiagomissiato.xpmarvel.model.Collection
import io.reactivex.Flowable

/**
 * Created by tiagomissiato on 6/6/18.
 */
open class CharacterDetailRepositoryMock : CharacterDetailContract.Repository {

    override fun getDetail(characterId: Int?): Flowable<CharacterListResponse> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getComics(characterId: Int?): Flowable<ComicDataWrapper> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getCollection(url: String?): Flowable<ComicDataWrapper> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}