package com.tiagomissiato.xpmarvel.characterdetail

import android.content.Context
import com.tiagomissiato.xpmarvel.model.Character
import com.tiagomissiato.xpmarvel.model.Item
import io.reactivex.functions.Action
import org.junit.Before
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

/**
 * Created by tiagomissiato on 6/6/18.
 */
open class FakeCharacterDetailView : CharacterDetailContract.View {

    private var showErrorStateAction: Action = Mockito.mock(Action::class.java)
    private var hideErrorStateAction: Action = Mockito.mock(Action::class.java)
    private var showLoadingStateAction: Action = Mockito.mock(Action::class.java)
    private var hideLoadingStateAction: Action = Mockito.mock(Action::class.java)

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    override fun showErrorState(): Action = showErrorStateAction

    override fun showLoading(): Action = showLoadingStateAction

    override fun hideLoading(): Action = hideLoadingStateAction

    override fun hideErrorState(): Action = hideErrorStateAction

    override fun showDetail(character: Character) { }

    override fun showCollection(title: String, collection: List<Item>) { }

    override fun getContext(): Context? = Mockito.mock(Context::class.java)
}