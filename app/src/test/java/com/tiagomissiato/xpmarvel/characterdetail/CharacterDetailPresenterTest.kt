package com.tiagomissiato.xpmarvel.characterdetail

import com.tiagomissiato.xpmarvel.AppUnitTest
import com.tiagomissiato.xpmarvel.characterlist.CharacterListContract
import com.tiagomissiato.xpmarvel.characterlist.CharacterListPresenter
import com.tiagomissiato.xpmarvel.characterlist.CharacterListRepositoryMock
import com.tiagomissiato.xpmarvel.characterlist.FakeCharacterListView
import com.tiagomissiato.xpmarvel.core.lifecycle.LifecycleStrategist
import com.tiagomissiato.xpmarvel.model.CharacterListResponse
import io.reactivex.Flowable
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito

/**
 * Created by tiagomissiato on 6/6/18.
 */
class CharacterDetailPresenterTest : AppUnitTest() {


    private var strategist: LifecycleStrategist = Mockito.mock(LifecycleStrategist::class.java)

    private lateinit var view: CharacterDetailContract.View
    private lateinit var repository: CharacterDetailRepositoryMock
    private lateinit var presenter: CharacterDetailPresenter

    override fun setup() {
        super.setup()

        view = Mockito.spy(FakeCharacterDetailView())
        repository = CharacterDetailRepositoryMock()
        presenter = CharacterDetailPresenter(view, strategist, repository, ioScheduler, uiScheduler)
    }

}