package com.tiagomissiato.xpmarvel.characterlist

import com.tiagomissiato.xpmarvel.AppUnitTest
import com.tiagomissiato.xpmarvel.core.lifecycle.LifecycleStrategist
import com.tiagomissiato.xpmarvel.model.CharacterListResponse
import io.reactivex.Flowable
import org.junit.Test
import org.mockito.ArgumentMatchers
import org.mockito.Mockito.*

/**
 * Created by tiagomissiato on 6/6/18.
 */
class CharacterListPresenterTest : AppUnitTest() {


    private var strategist: LifecycleStrategist = mock(LifecycleStrategist::class.java)

    private lateinit var view: CharacterListContract.View
    private lateinit var repository: CharacterListRepositoryMock
    private lateinit var presenter: CharacterListPresenter

    override fun setup() {
        super.setup()

        view = spy(FakeCharacterListView())
        repository = CharacterListRepositoryMock()
        presenter = CharacterListPresenter(view, strategist, repository, ioScheduler, uiScheduler)
    }

    @Test
    fun `should_show_character` () {
        presenter.getCharacterList()
        verify(view, times(1)).showCharacterList(ArgumentMatchers.anyList())

        verify(view.hideErrorState(), times(1)).run()
        verify(view.showLoading(), times(1)).run()
        verify(view.hideLoading(), times(1)).run()
        verify(view.hideEmptyState(), times(1)).run()
    }

    @Test
    fun `should_show_one_page_20_character` () {
        repository.numCharacter = 20
        presenter.getCharacterList()
        presenter.getCharacterList(1)
        verify(view, times(1)).showCharacterList(ArgumentMatchers.anyList())

        verify(view.hideErrorState(), times(2)).run()
        verify(view.showLoading(), times(2)).run()
        verify(view.hideLoading(), times(2)).run()
        verify(view.hideEmptyState(), times(1)).run()
    }

    @Test
    fun `should_show_two_page_40_character` () {
        repository.numCharacter = 40
        presenter.getCharacterList()
        presenter.getCharacterList(1)
        presenter.getCharacterList(2)

        verify(view, times(2)).showCharacterList(ArgumentMatchers.anyList())
        verify(view.hideErrorState(), times(3)).run()
        verify(view.showLoading(), times(3)).run()
        verify(view.hideLoading(), times(3)).run()
        verify(view.hideEmptyState(), times(2)).run()
    }

    @Test
    fun `should_show_three_page_45_character` () {
        repository.numCharacter = 45
        presenter.getCharacterList()
        presenter.getCharacterList(1)
        presenter.getCharacterList(2)

        verify(view, times(3)).showCharacterList(ArgumentMatchers.anyList())
        verify(view.hideErrorState(), times(3)).run()
        verify(view.showLoading(), times(3)).run()
        verify(view.hideLoading(), times(3)).run()
        verify(view.hideEmptyState(), times(3)).run()
    }

    @Test
    fun `should_show_three_page_45_character_four_scroll` () {
        repository.numCharacter = 45
        presenter.getCharacterList()
        presenter.getCharacterList(1)
        presenter.getCharacterList(2)
        presenter.getCharacterList(3)

        verify(view, times(3)).showCharacterList(ArgumentMatchers.anyList())
        verify(view.hideErrorState(), times(4)).run()
        verify(view.showLoading(), times(4)).run()
        verify(view.hideLoading(), times(4)).run()
        verify(view.hideEmptyState(), times(3)).run()
    }

    @Test
    fun `should_show_empty_state` () {
        repository.numCharacter = 0
        presenter.getCharacterList()

        verify(view, never()).showCharacterList(ArgumentMatchers.anyList())
        verify(view.hideErrorState(), times(1)).run()
        verify(view.showLoading(), times(1)).run()
        verify(view.hideLoading(), times(1)).run()
        verify(view.showEmptyState(), times(1)).run()
        verify(view.hideEmptyState(), never()).run()
    }

    @Test
    fun `should_show_error_state` () {
        val error: Flowable<CharacterListResponse> = Flowable.error(Exception("Exception"))

        val repositoryError = mock(CharacterListRepositoryMock::class.java)
        val presenterError = CharacterListPresenter(view, strategist, repositoryError, ioScheduler, uiScheduler)

        doReturn(error).`when`(repositoryError).getCharacter(ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt(), ArgumentMatchers.anyInt())

        repositoryError.numCharacter = 0
        presenterError.getCharacterList()

        verify(view, never()).showCharacterList(ArgumentMatchers.anyList())
        verify(view.showLoading(), times(1)).run()
        verify(view.hideLoading(), times(1)).run()
        verify(view.hideErrorState(), times(1)).run()
        verify(view.showErrorState(), times(1)).run()
        verify(view.showEmptyState(), never()).run()
        verify(view.hideEmptyState(), never()).run()
    }
}