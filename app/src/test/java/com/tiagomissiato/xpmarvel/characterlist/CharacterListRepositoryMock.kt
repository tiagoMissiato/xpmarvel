package com.tiagomissiato.xpmarvel.characterlist

import com.tiagomissiato.xpmarvel.model.*
import com.tiagomissiato.xpmarvel.model.Collection
import io.reactivex.Flowable

/**
 * Created by tiagomissiato on 6/6/18.
 */
open class CharacterListRepositoryMock : CharacterListContract.Repository {

    var numCharacter: Int = 20

    override fun getCharacter(type: Int, offset: Int, limit: Int): Flowable<CharacterListResponse> {
        val characterList = when(numCharacter) {
            0 -> {
                mutableListOf()
            }
            else -> {
                (1 until numCharacter).map {
                    Character(it, "Name_$it",
                            "Description_$it",
                            Thumbnail("path_$it", "jpg"),
                            Collection(0, emptyList()),
                            Collection(0, emptyList()),
                            Collection(0, emptyList()),
                            Collection(0, emptyList()))
                }
            }
        }

        return Flowable.just(
                CharacterListResponse(
                        data = CharacterDataContainer(
                                results = getPaged(characterList, offset, limit),
                                offset = 0)
                )
        )
    }

    private fun getPaged(list: List<Character>, offset: Int, limit: Int): List<Character> {
        return when {
            offset == 0 && limit == 0 -> list
            offset > list.size -> mutableListOf()
            offset + limit > list.size -> list.slice(offset until list.size).toMutableList()
            else -> list.slice(offset..offset.plus(limit)).toMutableList()
        }
    }

}