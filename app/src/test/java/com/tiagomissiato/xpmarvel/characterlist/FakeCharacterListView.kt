package com.tiagomissiato.xpmarvel.characterlist

import android.content.Context
import com.tiagomissiato.xpmarvel.model.Character
import io.reactivex.functions.Action
import org.junit.Before
import org.mockito.Mockito.mock
import org.mockito.MockitoAnnotations

/**
 * Created by tiagomissiato on 6/6/18.
 */
open class FakeCharacterListView : CharacterListContract.View {
    private var showErrorStateAction: Action = mock(Action::class.java)
    private var hideErrorStateAction: Action = mock(Action::class.java)
    private var showLoadingStateAction: Action = mock(Action::class.java)
    private var hideLoadingStateAction: Action = mock(Action::class.java)
    private var hideEmptyStateAction: Action = mock(Action::class.java)
    private var showEmptyStateAction: Action = mock(Action::class.java)

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
    }

    override fun showErrorState(): Action = showErrorStateAction

    override fun showLoading(): Action = showLoadingStateAction

    override fun hideLoading(): Action = hideLoadingStateAction

    override fun hideErrorState(): Action = hideErrorStateAction

    override fun showEmptyState(): Action = showEmptyStateAction

    override fun hideEmptyState(): Action = hideEmptyStateAction

    override fun showCharacterList(characterList: List<Character>) { }

    override fun getContext(): Context? = mock(Context::class.java)

    override fun clear() { }

    override fun disableRefresh() { }

    override fun enableRefresh() { }
}