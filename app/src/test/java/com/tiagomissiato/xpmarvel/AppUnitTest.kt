package com.tiagomissiato.xpmarvel

import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.mockito.MockitoAnnotations

/**
 * Created by tiagomissiato on 6/6/18.
 */
abstract class AppUnitTest {

    internal var uiScheduler = Schedulers.trampoline()
    internal var ioScheduler = Schedulers.trampoline()

    @Before
    open fun setup() {
        MockitoAnnotations.initMocks(this)
    }

}