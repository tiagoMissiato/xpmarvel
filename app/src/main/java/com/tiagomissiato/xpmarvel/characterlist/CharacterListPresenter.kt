package com.tiagomissiato.xpmarvel.characterlist

import com.tiagomissiato.xpmarvel.core.behaviour.ErrorStateBehaviour
import com.tiagomissiato.xpmarvel.core.behaviour.LoadingStateBehaviour
import com.tiagomissiato.xpmarvel.core.lifecycle.LifecycleStrategist
import com.tiagomissiato.xpmarvel.core.mvp.BasePresenter
import com.tiagomissiato.xpmarvel.extention.fire
import com.tiagomissiato.xpmarvel.model.Character
import io.reactivex.Scheduler

class CharacterListPresenter(view: CharacterListContract.View,
                             strategist: LifecycleStrategist,
                             private val repository: CharacterListContract.Repository,
                             private val ioScheduler: Scheduler,
                             private val uiScheduler: Scheduler)
    : BasePresenter<CharacterListContract.View>(view, strategist),
        CharacterListContract.UseCase {

    var pageSize: Int = 20
    var source: Int =  CharacterListRepository.Source.API
    var makeRequest: Boolean = true
    var bufferedCharacter: MutableList<Character> = mutableListOf()
    var filteringMode: Boolean = false

    override fun getCharacterList(page: Int) {
        if ((!makeRequest && page != 0) || filteringMode) return

        val offset = page * pageSize

        addDisposable(repository.getCharacter(source, offset, pageSize)
                .subscribeOn(ioScheduler)
                .observeOn(uiScheduler)
                .compose(LoadingStateBehaviour(view, uiScheduler))
                .compose(ErrorStateBehaviour(view, uiScheduler))
                .subscribe ({
                    if (it.data.results.isEmpty()) {
                        makeRequest = false
                        if (page == 0) view.showEmptyState().fire(uiScheduler)
                    } else {
                        view.hideEmptyState().fire(uiScheduler)
                        view.showCharacterList(it.data.results)

                        if (page == 0) bufferedCharacter.clear()

                        bufferedCharacter.addAll(it.data.results)
                    }
                }, { view.showErrorState().fire(uiScheduler) } )
        )
    }

    override fun filter(term: String) {
        view.clear()
        view.hideEmptyState().fire(uiScheduler)
        if (term.isEmpty()) {
            filteringMode = false
            view.enableRefresh()
            view.showCharacterList(bufferedCharacter)
        } else {
            filteringMode = true
            view.disableRefresh()
            val newList = bufferedCharacter.filter { it.name.toLowerCase().startsWith(term.toLowerCase()) }
            view.showCharacterList(newList)
            if (newList.isEmpty()) view.showEmptyState().fire(uiScheduler)
        }
    }


}