package com.tiagomissiato.xpmarvel.characterlist

import com.tiagomissiato.xpmarvel.core.mvp.BaseContract
import com.tiagomissiato.xpmarvel.core.mvp.EmptyStateView
import com.tiagomissiato.xpmarvel.core.mvp.ErrorStateView
import com.tiagomissiato.xpmarvel.core.mvp.LoadingStateView
import com.tiagomissiato.xpmarvel.model.Character
import com.tiagomissiato.xpmarvel.model.CharacterListResponse
import io.reactivex.Flowable

interface CharacterListContract {

    interface View : BaseContract.View, LoadingStateView, ErrorStateView, EmptyStateView {
        fun showCharacterList(characterList: List<Character>)
        fun clear()
        fun disableRefresh()
        fun enableRefresh()
    }

    interface UseCase: BaseContract.UseCase {
        fun getCharacterList(page: Int = 0)
        fun filter(term: String)
    }

    interface Repository {
        fun getCharacter(type: Int = CharacterListRepository.Source.API
                         , offset: Int = 0
                         , limit: Int = 20): Flowable<CharacterListResponse>
    }
}