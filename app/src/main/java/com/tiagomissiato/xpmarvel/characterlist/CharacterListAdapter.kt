package com.tiagomissiato.xpmarvel.characterlist

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.support.v7.graphics.Palette
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.tiagomissiato.xpmarvel.R
import com.tiagomissiato.xpmarvel.helper.PreferenceHelper
import com.tiagomissiato.xpmarvel.model.Character
import kotlinx.android.synthetic.main.list_item_character.view.*
import java.lang.Exception

class CharacterListAdapter(private val inflater: LayoutInflater,
                           private val openListener: (Character) -> Unit,
                           private val favoriteListener: (Character) -> Unit)
    : RecyclerView.Adapter<CharacterListAdapter.ViewHolder>() {

    var items: MutableList<Character>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterListAdapter.ViewHolder {
        val view = inflater.inflate(R.layout.list_item_character, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items?.get(position), openListener, favoriteListener)
    }

    fun addItems(characterList: List<Character>, prefs: PreferenceHelper? = null) {
        val saved = prefs?.getCharacters() ?: mutableListOf()
        saved.forEach { s ->
            characterList.find { r -> r.id == s.id }?.favorite = true
        }

        if (items == null) {
            items = characterList.toMutableList()
        } else {
            items?.addAll(characterList.toMutableList())
        }
        items?.distinctBy { it.id }
        notifyDataSetChanged()
    }

    fun clear() {
        items?.clear()
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Character?, listener: (Character) -> Unit, favoriteListener: (Character) -> Unit) = with(itemView) {
            item?.let {
                characterContainer.setBackgroundColor(Color.GRAY)
                Picasso.get().load(item.thumbnail.toString()).into(object : Target {
                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                        bitmap?.let {
                            characterImage.setImageBitmap(bitmap)
                            Palette.from(bitmap).generate { palette ->
                                characterContainer.setBackgroundColor(palette.vibrantSwatch?.rgb ?: Color.GRAY)
                                characterName.setTextColor(palette.vibrantSwatch?.titleTextColor ?: Color.WHITE)
                            }
                        }
                    }

                    override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) { }

                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
                })
                characterName.text = item.name

                characterFavorite.isSelected = item.favorite
                characterFavorite.setOnClickListener {
                    item.favorite = !item.favorite
                    item.let { favoriteListener(it) }
                    it.isSelected = item.favorite
                }
                setOnClickListener {
                    item.let { listener(it) }
                }
            }
        }
    }
}