package com.tiagomissiato.xpmarvel.characterlist

import com.tiagomissiato.xpmarvel.characterlist.CharacterListRepository.Source.API
import com.tiagomissiato.xpmarvel.characterlist.CharacterListRepository.Source.LOCAL
import com.tiagomissiato.xpmarvel.core.MarvelApi
import com.tiagomissiato.xpmarvel.extention.toResponseFlowable
import com.tiagomissiato.xpmarvel.model.CharacterListResponse
import com.tiagomissiato.xpmarvel.helper.PreferenceHelper
import io.reactivex.Flowable
import javax.inject.Inject

open class CharacterListRepository @Inject constructor(private var api: MarvelApi,
                                                       private var prefs: PreferenceHelper)
    : CharacterListContract.Repository {

    object Source {
        const val API: Int = 0
        const val LOCAL: Int = 1
    }

    override fun getCharacter(type: Int, offset: Int, limit: Int): Flowable<CharacterListResponse>
            = when(type) {
                LOCAL -> {
                    prefs.getCharacters(offset, limit).toResponseFlowable()
                }
                else -> {
                    api.getCharacters(offset, limit)
                }
            }
}