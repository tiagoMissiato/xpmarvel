package com.tiagomissiato.xpmarvel.characterlist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.tiagomissiato.xpmarvel.R
import com.tiagomissiato.xpmarvel.characterdetail.CharacterDetailActivity
import com.tiagomissiato.xpmarvel.core.AppFragment
import com.tiagomissiato.xpmarvel.core.Communication
import com.tiagomissiato.xpmarvel.core.lifecycle.LifecycleStrategist
import com.tiagomissiato.xpmarvel.extention.hasTwoPanes
import com.tiagomissiato.xpmarvel.extention.hide
import com.tiagomissiato.xpmarvel.extention.show
import com.tiagomissiato.xpmarvel.helper.EndlessRecyclerScrollListener
import com.tiagomissiato.xpmarvel.helper.PreferenceHelper
import com.tiagomissiato.xpmarvel.model.Character
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Action
import kotlinx.android.synthetic.main.fragment_character_list.*
import kotlinx.android.synthetic.main.fragment_character_list.view.*
import javax.inject.Inject


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [CharacterListFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [CharacterListFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class CharacterListFragment : AppFragment(), CharacterListContract.View {

    @Inject
    lateinit var presenter: CharacterListPresenter

    @Inject
    lateinit var prefs: PreferenceHelper

    @Inject
    lateinit var strategist: LifecycleStrategist

    @Inject
    lateinit var communication: Communication

    lateinit var adapter: CharacterListAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_character_list, container, false)

        adapter = CharacterListAdapter(inflater, { open ->

            if (resources.hasTwoPanes()) {
                communication.postCharacterClicked(open.id)
            } else {
                startActivity(Intent(context, CharacterDetailActivity::class.java).apply {
                    putExtras(bundleOf("id" to open.id))
                })
            }

        }, { save ->
            prefs.addRemoveCharacter(save)
        })

        val layoutManager = LinearLayoutManager(context)
        view.characterList.layoutManager = layoutManager
        view.characterList.adapter = adapter
        view.characterList.addOnScrollListener(EndlessRecyclerScrollListener(layoutManager) {
            Log.i("DEBUG", "page: $it")
            presenter.getCharacterList(it)
        })

        view.pullRefresh.setOnRefreshListener {
            presenter.getCharacterList()
            adapter.clear()

        }
        presenter.getCharacterList()
//        showEmptyState().fire(AndroidSchedulers.mainThread())

        val disposable = communication.getSearchObserver()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
            presenter.filter(it)
        }
        strategist.applyStrategy(disposable)
        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

         presenter.source = arguments?.get("type") as Int? ?: CharacterListRepository.Source.API
    }

    companion object {
        fun newInstance(type: Int = CharacterListRepository.Source.API) = CharacterListFragment().apply {
            arguments = bundleOf("type" to type)
        }
    }

    override fun clear() {
        adapter.clear()
    }
    override fun showCharacterList(characterList: List<Character>) {
        adapter.addItems(characterList, prefs)
    }

    override fun disableRefresh() {
        pullRefresh.isEnabled = false
    }

    override fun enableRefresh() {
        pullRefresh.isEnabled = true
    }

    override fun showLoading(): Action = Action { pullRefresh.isRefreshing = true }

    override fun hideLoading(): Action = Action { pullRefresh.isRefreshing = false }

    override fun showErrorState(): Action = Action {
        characterList.hide()
        emptyError.error {
            presenter.getCharacterList()
        }
    }

    override fun hideErrorState(): Action = Action {
        characterList.show()
        emptyError.hide()
    }

    override fun showEmptyState(): Action = Action {
        characterList.hide()
        emptyError.empty()
    }

    override fun hideEmptyState(): Action = Action {
        characterList.show()
        emptyError.hide()
    }
}
