package com.tiagomissiato.xpmarvel.helper

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView


/**
 * Created by tiagomissiato on 6/5/18.
 */
open class EndlessRecyclerScrollListener(private val linearLayoutManager: LinearLayoutManager
                                         , private val loadListener: (Int) -> Unit)
    : RecyclerView.OnScrollListener() {

    // The total number of items in the dataset after the last load
    private var previousTotal = 0
    // True if we are still waiting for the last set of data to load.
    private var loading = true
    private var currentPage = 0

    override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val visibleItemCount = recyclerView!!.childCount
        val totalItemCount = linearLayoutManager.itemCount
        val firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition()

        //The recycler was refreshed and need to reset to call onLoadMore again
        if (previousTotal > totalItemCount) reset()

        if (loading) {
            if (totalItemCount > previousTotal) {
                loading = false
                previousTotal = totalItemCount
            }
        }
        val visibleThreshold = 5
        if (!loading && totalItemCount - visibleItemCount <= firstVisibleItem + visibleThreshold) {
            currentPage++
            loading = true
            loadListener(currentPage)
        }
    }

    private fun reset() {
        previousTotal = 0
        currentPage = 1
        loading = true
    }
}