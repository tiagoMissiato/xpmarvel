package com.tiagomissiato.xpmarvel.helper

import android.content.SharedPreferences
import com.tiagomissiato.xpmarvel.extention.toCharacterList
import com.tiagomissiato.xpmarvel.extention.toJson
import com.tiagomissiato.xpmarvel.helper.PreferenceHelper.Constant.SHARED_CHARACTER_LIST
import com.tiagomissiato.xpmarvel.model.Character
import javax.inject.Inject

/**
 * Created by tiagomissiato on 6/5/18.
 */
class PreferenceHelper @Inject constructor(private val prefs: SharedPreferences) {

    object Constant {
        val SHARED_CHARACTER_LIST = "com.tiagomissiato.xpmarvel.preference.SHARED_CHARACTER_LIST"
    }

    fun getCharacters(offset: Int = 0, limit: Int = 0): MutableList<Character> {

        val list = prefs.getString(SHARED_CHARACTER_LIST, "")
                .toCharacterList().sortedBy { it.name }.toMutableList()

        return when {
            offset == 0 && limit == 0 -> list
            offset > list.size -> mutableListOf()
            offset + limit > list.size -> list.slice(offset until list.size).toMutableList()
            else -> list.slice(offset..offset.plus(limit)).toMutableList()
        }
    }

    fun addRemoveCharacter(character: Character) {
        val newList: MutableList<Character> = getCharacters()
        if (character.favorite) {
            newList.add(character)
        } else {
            newList.removeAll { it.id == character.id }
        }
        prefs.edit().putString(
                SHARED_CHARACTER_LIST,
                newList.distinctBy { it.id }.toMutableList().toJson()
        ).apply()
    }

    fun getCharacter(characterId: Int?): Character?
            = getCharacters().firstOrNull { it.id == characterId }

    fun isFavorite(id: Int): Boolean = getCharacters().any { it.id == id }
}