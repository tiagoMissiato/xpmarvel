package com.tiagomissiato.xpmarvel.characterdetail

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.support.v7.graphics.Palette
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import com.squareup.picasso.Picasso.LoadedFrom
import com.squareup.picasso.Target
import com.tiagomissiato.xpmarvel.R
import kotlinx.android.synthetic.main.list_item_comic.view.*
import java.lang.Exception


class CharacterComicsAdapter(private val inflater: LayoutInflater) : RecyclerView.Adapter<CharacterComicsAdapter.ViewHolder>() {

    var items: List<Pair<String, String>>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterComicsAdapter.ViewHolder {
        val view = inflater.inflate(R.layout.list_item_comic, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items?.size ?: 0
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items?.get(position))
    }

    fun addItems(comics: List<Pair<String, String>>) {
        items = comics
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View?) : RecyclerView.ViewHolder(itemView) {
        fun bind(item: Pair<String, String>?) = with(itemView) {
            item?.let {
                val (name, thumbs) = item
                Picasso.get().load(thumbs).placeholder(R.drawable.marvel_placeholder).into(object : Target {
                    override fun onBitmapLoaded(bitmap: Bitmap?, from: LoadedFrom?) {
                        bitmap?.let {
                            comicImage.setImageBitmap(bitmap)
                            Palette.from(bitmap).generate { palette ->
                                comicsNameContainer.setBackgroundColor(palette.vibrantSwatch?.rgb ?: Color.BLACK)
                                comicName.setTextColor(palette.vibrantSwatch?.titleTextColor ?: Color.WHITE)
                            }
                        }
                    }

                    override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                        comicImage.setBackgroundResource(R.drawable.marvel_placeholder)
                        comicsNameContainer.setBackgroundColor(Color.BLACK)
                        comicName.setTextColor(Color.WHITE)
                    }

                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
                })
//                Picasso.get().load(thumbs).into(comicImage)
                comicName.text = name
            }
        }
    }
}