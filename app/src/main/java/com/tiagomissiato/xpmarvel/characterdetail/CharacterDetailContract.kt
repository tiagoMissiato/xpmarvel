package com.tiagomissiato.xpmarvel.characterdetail

import com.tiagomissiato.xpmarvel.core.mvp.BaseContract
import com.tiagomissiato.xpmarvel.core.mvp.ErrorStateView
import com.tiagomissiato.xpmarvel.core.mvp.LoadingStateView
import com.tiagomissiato.xpmarvel.model.Character
import com.tiagomissiato.xpmarvel.model.CharacterListResponse
import com.tiagomissiato.xpmarvel.model.ComicDataWrapper
import com.tiagomissiato.xpmarvel.model.Item
import io.reactivex.Flowable

interface CharacterDetailContract {

    interface View : BaseContract.View, LoadingStateView, ErrorStateView {
        fun showDetail(character: Character)
        fun showCollection(title: String, collection: List<Item>)
    }

    interface UseCase : BaseContract.UseCase {
        fun getDetail(characterId: Int?)
        fun getCharacter(): Character
    }

    interface Repository {
        fun getDetail(characterId: Int?): Flowable<CharacterListResponse>
        fun getComics(characterId: Int?): Flowable<ComicDataWrapper>
        fun getCollection(url: String?): Flowable<ComicDataWrapper>
    }
}