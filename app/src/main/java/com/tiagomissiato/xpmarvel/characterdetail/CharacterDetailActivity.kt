package com.tiagomissiato.xpmarvel.characterdetail

import android.os.Bundle
import android.view.MenuItem
import com.squareup.picasso.Picasso
import com.tiagomissiato.xpmarvel.R
import com.tiagomissiato.xpmarvel.core.AppActivity
import com.tiagomissiato.xpmarvel.core.Communication
import com.tiagomissiato.xpmarvel.core.lifecycle.LifecycleStrategist
import com.tiagomissiato.xpmarvel.extention.fragmentManager
import kotlinx.android.synthetic.main.activity_character_detail.*
import javax.inject.Inject

class CharacterDetailActivity : AppActivity() {

    @Inject
    lateinit var communication: Communication

    @Inject
    lateinit var strategist: LifecycleStrategist

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character_detail)

        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.title = null
        }

        strategist.applyStrategy(communication.getCharacterObserver().subscribe {
            Picasso.get().load(it).into(imageHeader)
        })

        fragmentManager {
            add(R.id.content, CharacterDetailFragment.newInstance(intent.extras.getInt("id")))
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
