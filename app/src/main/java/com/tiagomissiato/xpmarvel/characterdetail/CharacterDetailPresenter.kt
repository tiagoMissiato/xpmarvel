package com.tiagomissiato.xpmarvel.characterdetail

import com.tiagomissiato.xpmarvel.R
import com.tiagomissiato.xpmarvel.core.behaviour.ErrorStateBehaviour
import com.tiagomissiato.xpmarvel.core.behaviour.LoadingStateBehaviour
import com.tiagomissiato.xpmarvel.core.lifecycle.LifecycleStrategist
import com.tiagomissiato.xpmarvel.core.mvp.BasePresenter
import com.tiagomissiato.xpmarvel.extention.fire
import com.tiagomissiato.xpmarvel.model.Character
import io.reactivex.Scheduler

class CharacterDetailPresenter(view: CharacterDetailContract.View,
                               strategist: LifecycleStrategist,
                               private val repository: CharacterDetailContract.Repository,
                               private val ioScheduler: Scheduler,
                               private val uiScheduler: Scheduler)
    : BasePresenter<CharacterDetailContract.View>(view, strategist), CharacterDetailContract.UseCase {

    lateinit var bufferedCharacter: Character

    override fun getDetail(characterId: Int?) {
        addDisposable(repository.getDetail(characterId)
                .subscribeOn(ioScheduler)
                .observeOn(uiScheduler)
                .compose(LoadingStateBehaviour(view, uiScheduler))
                .compose(ErrorStateBehaviour(view, uiScheduler))
                .subscribe ({
                    if (it.data.results.isNotEmpty()) {
                        bufferedCharacter = it.data.results.first()
                        view.showDetail(bufferedCharacter)
                        val rs = view.getContext()
                        if (bufferedCharacter.comics.items.isNotEmpty()) {
                                view.showCollection(rs?.getString(R.string.collection_comics) ?: "",
                                        bufferedCharacter.comics.items)
                        }
                        if (bufferedCharacter.series.items.isNotEmpty()) {
                                view.showCollection(rs?.getString(R.string.collection_series) ?: "",
                                        bufferedCharacter.series.items)
                        }
                        if (bufferedCharacter.stories.items.isNotEmpty()) {
                                view.showCollection(rs?.getString(R.string.collection_stories) ?: "",
                                        bufferedCharacter.stories.items)
                        }
                        if (bufferedCharacter.events.items.isNotEmpty()) {
                                view.showCollection(rs?.getString(R.string.collection_events) ?: "",
                                        bufferedCharacter.events.items)
                        }
                    }
                }, { view.showErrorState().fire(uiScheduler) } )
        )
    }

    override fun getCharacter(): Character = bufferedCharacter
}