package com.tiagomissiato.xpmarvel.characterdetail

import com.tiagomissiato.xpmarvel.core.MarvelApi
import com.tiagomissiato.xpmarvel.extention.toListResponse
import com.tiagomissiato.xpmarvel.helper.PreferenceHelper
import com.tiagomissiato.xpmarvel.model.CharacterListResponse
import io.reactivex.Flowable
import javax.inject.Inject

class CharacterDetailRepository @Inject constructor(var api: MarvelApi,
                                                    private var prefs: PreferenceHelper)
    : CharacterDetailContract.Repository {

    override fun getDetail(characterId: Int?): Flowable<CharacterListResponse> {
        val character = prefs.getCharacter(characterId)
        return character?.toListResponse() ?: api.getCharacterDetail(characterId)
    }

    override fun getComics(characterId: Int?) = api.getComics(characterId)

    override fun getCollection(url: String?) = api.getCollection(url)
}