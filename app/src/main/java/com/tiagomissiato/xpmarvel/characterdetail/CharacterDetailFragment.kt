package com.tiagomissiato.xpmarvel.characterdetail

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import com.squareup.picasso.Picasso
import com.tiagomissiato.xpmarvel.R
import com.tiagomissiato.xpmarvel.charactercollection.CharacterCollectionView
import com.tiagomissiato.xpmarvel.core.AppFragment
import com.tiagomissiato.xpmarvel.core.Communication
import com.tiagomissiato.xpmarvel.core.MarvelApi
import com.tiagomissiato.xpmarvel.core.lifecycle.LifecycleStrategist
import com.tiagomissiato.xpmarvel.extention.hasTwoPanes
import com.tiagomissiato.xpmarvel.extention.hide
import com.tiagomissiato.xpmarvel.extention.show
import com.tiagomissiato.xpmarvel.helper.PreferenceHelper
import com.tiagomissiato.xpmarvel.model.Character
import com.tiagomissiato.xpmarvel.model.Item
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Action
import kotlinx.android.synthetic.main.fragment_character_detail.*
import kotlinx.android.synthetic.main.fragment_character_detail.view.*
import javax.inject.Inject

class CharacterDetailFragment : AppFragment(), CharacterDetailContract.View {

    @Inject
    lateinit var presenter: CharacterDetailPresenter

    @Inject
    lateinit var communication: Communication

    @Inject
    lateinit var api: MarvelApi

    @Inject
    lateinit var prefs: PreferenceHelper

    @Inject
    lateinit var strategist: LifecycleStrategist

    var adapter: CharacterComicsAdapter? = null
    var characterId: Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_character_detail, container, false)

        adapter = CharacterComicsAdapter(inflater)

        characterId?.let {
            presenter.getDetail(characterId)
        }

        view.favoriteBtn.setOnClickListener {
            presenter.bufferedCharacter.favorite = !presenter.bufferedCharacter.favorite
            favoriteBtn.isSelected = presenter.bufferedCharacter.favorite
            prefs.addRemoveCharacter(presenter.getCharacter())
        }

        strategist.applyStrategy(communication.getCharacterClickedObserver()
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    presenter.getDetail(it)
        })

        if (context?.resources?.hasTwoPanes() == true) {
            view.detailEmptyError.custom("Personagem", "Selecione um ppersonagem para visualizar os detalhes.")
        }

        return view
    }


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        characterId = arguments?.get("id") as Int?
    }

    companion object {
        fun newInstance(id: Int)
                = CharacterDetailFragment().apply {
                    arguments = bundleOf("id" to id)
                }
    }

    override fun showDetail(character: Character) {
        if (resources.hasTwoPanes()) {
            Picasso.get().load(character.thumbnail.toString()).into(characterDetailImage)
            characterDetailImage.visibility = View.VISIBLE
        } else {
            communication.postCharacterImage(character.thumbnail.toString())
        }
        presenter.bufferedCharacter.favorite = prefs.isFavorite(character.id)
        characterName.text = character.name
        characterSubtitle.text = character.description
        favoriteBtn.visibility = View.VISIBLE
        favoriteBtn.isSelected = presenter.bufferedCharacter.favorite
        detailContent.show()
    }

    override fun showCollection(title: String, collection: List<Item>) {
        context?.let {
            view?.collectionsAux?.addView(CharacterCollectionView(it)
                    .api(api)
                    .bind(title, collection))
        }
    }

    override fun showLoading(): Action = Action {
        detailContent.hide()
        detailLoading.show()
    }

    override fun hideLoading(): Action = Action { detailLoading.hide() }

    override fun showErrorState(): Action = Action {
        detailContent.hide()
        detailEmptyError.error {
            presenter.getDetail(characterId)
        }
    }

    override fun hideErrorState(): Action = Action {
        detailContent.show()
        detailEmptyError.hide()
    }
}
