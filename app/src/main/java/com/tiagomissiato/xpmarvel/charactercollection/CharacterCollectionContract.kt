package com.tiagomissiato.xpmarvel.charactercollection

import com.tiagomissiato.xpmarvel.core.mvp.BaseContract
import com.tiagomissiato.xpmarvel.core.mvp.ErrorStateView
import com.tiagomissiato.xpmarvel.core.mvp.LoadingStateView
import com.tiagomissiato.xpmarvel.model.Item

interface CharacterCollectionContract {

    interface View : BaseContract.View, LoadingStateView, ErrorStateView {
        fun showCollection(collection: List<Pair<String, String>>)
    }

    interface UseCase : BaseContract.UseCase {
        fun getCollectionDetails(collection: List<Item>)
    }
}