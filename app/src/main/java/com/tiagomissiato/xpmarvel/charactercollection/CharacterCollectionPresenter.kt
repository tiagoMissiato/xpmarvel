package com.tiagomissiato.xpmarvel.charactercollection

import com.tiagomissiato.xpmarvel.core.lifecycle.LifecycleStrategist
import com.tiagomissiato.xpmarvel.core.mvp.BasePresenter
import com.tiagomissiato.xpmarvel.extention.fire
import com.tiagomissiato.xpmarvel.extention.toPairList
import com.tiagomissiato.xpmarvel.model.Item
import io.reactivex.Observable
import io.reactivex.Scheduler
import javax.inject.Inject

class CharacterCollectionPresenter @Inject constructor(view: CharacterCollectionContract.View,
                                   strategist: LifecycleStrategist,
                                   private val repository: CharacterCollectionRepository,
                                   private val ioScheduler: Scheduler,
                                   private val uiScheduler: Scheduler)
    : BasePresenter<CharacterCollectionContract.View>(view, strategist), CharacterCollectionContract.UseCase {

    override fun getCollectionDetails(collection: List<Item>) {
        val disposable = Observable.fromIterable(collection)
                .compose {
                    it.doOnSubscribe {
                        view.showLoading().fire(uiScheduler)
                        view.hideErrorState().fire(uiScheduler)
                    }
                    it.doOnError {
                        view.showCollection(collection.toPairList())
                    }
                    it.doOnTerminate { view.hideLoading().fire(uiScheduler) }
                }
                .flatMap {
            repository.getCollection(it.resourceURI)
                    .doOnError {
                        view.showCollection(collection.toPairList())
                    }
                    .toObservable()
        }.map {
            val item = it.data?.results?.first()
            Pair(item?.title ?: "Unknown", item?.thumbnail?.toString() ?: "")
        }.toList().subscribeOn(ioScheduler)
                .observeOn(uiScheduler).subscribe ({ c ->
                view.showCollection(c.filter {
                    it.first.isEmpty().not() && it.second.isEmpty().not()
                } )
        }, { view.hideLoading().fire(uiScheduler) })

        addDisposable(disposable)
    }

}