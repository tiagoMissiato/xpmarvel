package com.tiagomissiato.xpmarvel.charactercollection

import com.tiagomissiato.xpmarvel.core.MarvelApi
import javax.inject.Inject

/**
 * Created by tiagomissiato on 6/4/18.
 */
class CharacterCollectionRepository @Inject constructor(var api: MarvelApi){
    fun getCollection(url: String) = api.getCollection(url)
}