package com.tiagomissiato.xpmarvel.charactercollection

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.tiagomissiato.xpmarvel.R
import com.tiagomissiato.xpmarvel.characterdetail.CharacterComicsAdapter
import com.tiagomissiato.xpmarvel.core.AppActivity
import com.tiagomissiato.xpmarvel.core.MarvelApi
import com.tiagomissiato.xpmarvel.core.lifecycle.DisposeStrategy
import com.tiagomissiato.xpmarvel.core.lifecycle.LifecycleStrategist
import com.tiagomissiato.xpmarvel.model.Item
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.view_character_collection.view.*

class CharacterCollectionView constructor(context: Context,
                                          attrs: AttributeSet? = null,
                                          defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr), CharacterCollectionContract.View {

    var adapter: CharacterComicsAdapter? = null

    private lateinit var presenter: CharacterCollectionPresenter

    init {
        LayoutInflater.from(context)
                .inflate(R.layout.view_character_collection, this, true)
    }

    fun api(api: MarvelApi): CharacterCollectionView {
        presenter = CharacterCollectionPresenter(this,
                LifecycleStrategist(context as AppActivity, DisposeStrategy()),
                CharacterCollectionRepository(api),
                Schedulers.io(),
                AndroidSchedulers.mainThread())
        return this
    }

    fun bind(title: String, collection: List<Item>): CharacterCollectionView {
        collectionName.text = title
        presenter.getCollectionDetails(collection)
        return this
    }

    override fun showCollection(collection: List<Pair<String, String>>) {
        adapter = CharacterComicsAdapter(LayoutInflater.from(context))
        adapter?.addItems(collection)
        collectionList?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        collectionList?.isNestedScrollingEnabled = false
        collectionList?.setHasFixedSize(true)
        collectionList?.adapter = adapter
    }

    override fun showLoading(): Action = Action { loading.visibility = View.VISIBLE }

    override fun hideLoading(): Action = Action { loading.visibility = View.GONE }

    override fun showErrorState(): Action = Action { visibility = GONE }

    override fun hideErrorState(): Action = Action { Log.i("DEBUG", "hideLoading") }
}