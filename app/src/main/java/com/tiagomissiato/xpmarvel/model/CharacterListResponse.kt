package com.tiagomissiato.xpmarvel.model


data class Item(var resourceURI: String, var name: String)

data class Collection(var available: Int, var items: List<Item>)

data class Thumbnail(var path: String, var extension: String) {
    override fun toString() = "$path.$extension"
}

data class Character(var id: Int,
                     var name: String,
                     var description: String,
                     var thumbnail: Thumbnail,
                     var comics: Collection,
                     var series: Collection,
                     var stories: Collection,
                     var events: Collection,
                     var favorite: Boolean = false)

data class CharacterDataContainer(var offset: Int, var results: List<Character>)

data class CharacterListResponse(var code: Int = 0, var status: String = "", var data: CharacterDataContainer)

data class Comic(var title: String?, var thumbnail: Thumbnail?)
data class ComicDataContainer(var results: List<Comic>)
data class ComicDataWrapper(var data: ComicDataContainer? = null)