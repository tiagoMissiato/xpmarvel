package com.tiagomissiato.xpmarvel.model

open class MarvelResponse<T>(var code: Int = 0, var status: String = "", var data: T? = null)