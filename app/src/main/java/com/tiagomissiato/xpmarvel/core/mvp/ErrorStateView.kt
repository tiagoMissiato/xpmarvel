package com.tiagomissiato.xpmarvel.core.mvp

import io.reactivex.functions.Action

interface ErrorStateView {
    fun showErrorState(): Action
    fun hideErrorState(): Action
}