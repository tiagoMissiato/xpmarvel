package com.tiagomissiato.xpmarvel.core

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import com.tiagomissiato.xpmarvel.R
import com.tiagomissiato.xpmarvel.extention.hide
import com.tiagomissiato.xpmarvel.extention.show
import kotlinx.android.synthetic.main.view_empty_error.view.*

class EmptyErrorView @JvmOverloads constructor(context: Context?,
                                 attrs: AttributeSet? = null,
                                 defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr){

    init {
        LayoutInflater.from(context)
                .inflate(R.layout.view_empty_error, this, true)
    }

    fun empty() {
        title.text = context.getString(R.string.empty_error_title)
        message.text = context.getString(R.string.empty_error_empty_message)
        retry.visibility = View.GONE
        show()
    }

    fun error(listener: (View) -> Unit) {
        title.text = context.getString(R.string.empty_error_title)
        message.text = context.getString(R.string.empty_error_error_message)
        retry.show()
        retry.setOnClickListener {
            listener(it)
        }
        show()
    }

    fun custom(titleStr: String, messageStr: String) {
        title.text = titleStr
        message.text = messageStr
        retry.hide()
        show()
    }
}