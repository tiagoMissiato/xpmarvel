package com.tiagomissiato.xpmarvel.core.behaviour

import com.tiagomissiato.xpmarvel.core.mvp.EmptyStateView
import com.tiagomissiato.xpmarvel.extention.fire
import io.reactivex.Flowable
import io.reactivex.FlowableTransformer
import io.reactivex.Scheduler
import org.reactivestreams.Publisher

class EmptyStateBehaviour<T>(val view: EmptyStateView,
                             private val uiScheduler: Scheduler) : FlowableTransformer<T, T> {

    override fun apply(upstream: Flowable<T>): Publisher<T> {
        return upstream.doOnSubscribe {
            view.showEmptyState().fire(uiScheduler)
        }.doOnTerminate {
            view.hideEmptyState().fire(uiScheduler)
        }
    }
}