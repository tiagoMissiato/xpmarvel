package com.tiagomissiato.xpmarvel.core

import android.support.v7.app.AppCompatActivity
import dagger.android.support.DaggerAppCompatActivity

abstract class AppActivity : DaggerAppCompatActivity()