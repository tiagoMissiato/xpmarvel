package com.tiagomissiato.xpmarvel.core.mvp

import io.reactivex.functions.Action

interface EmptyStateView {
    fun showEmptyState(): Action
    fun hideEmptyState(): Action
}