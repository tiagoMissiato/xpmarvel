package com.tiagomissiato.xpmarvel.core.lifecycle

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.util.Log
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

class DisposeStrategy : LifecycleObserver {

    private val composite = CompositeDisposable()

    fun addDisposable(toDispose: Disposable) {
        composite.add(toDispose)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        Log.i("DEBUG", "@OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)")
        composite.dispose()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun onResume() {
        Log.i("DEBUG", "@OnLifecycleEvent(Lifecycle.Event.ON_RESUME): ${composite.size()}")
    }
}