package com.tiagomissiato.xpmarvel.core.behaviour

import io.reactivex.Flowable
import io.reactivex.FlowableTransformer
import org.reactivestreams.Publisher

class Behaviours<T>(private val loading: LoadingStateBehaviour<T>,
                    private val networkError: ErrorStateBehaviour<T>) : FlowableTransformer<T, T> {

    override fun apply(upstream: Flowable<T>): Publisher<T> {
        return upstream
                .compose(loading)
                .compose(networkError)
    }

}