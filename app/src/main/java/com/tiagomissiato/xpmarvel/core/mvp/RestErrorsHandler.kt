package com.tiagomissiato.xpmarvel.core.mvp

import android.util.Log
import io.reactivex.Flowable
import io.reactivex.FlowableTransformer
import org.reactivestreams.Publisher
import org.reactivestreams.Subscription
import retrofit2.HttpException

class RestErrorsHandler<T> : FlowableTransformer<T, T> {

    override fun apply(upstream: Flowable<T>): Publisher<T> {
        return upstream
                .doOnSubscribe {
                    t: Subscription? -> Log.i("DEBUG", "teste")
                }
                .onErrorResumeNext(this::handleIfRestError)
    }


    private fun handleIfRestError(throwable: Throwable): Publisher<T> {
        return Flowable.error(throwable)
    }
}