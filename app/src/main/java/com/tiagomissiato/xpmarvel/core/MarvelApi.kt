package com.tiagomissiato.xpmarvel.core

import com.tiagomissiato.xpmarvel.model.CharacterListResponse
import com.tiagomissiato.xpmarvel.model.ComicDataWrapper
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import retrofit2.http.Url

interface MarvelApi {

    @GET("/v1/public/characters")
    fun getCharacters(@Query("offset") offset: Int = 0, @Query("limit") limit: Int = 20): Flowable<CharacterListResponse>

    @GET("/v1/public/characters/{characterId}")
    fun getCharacterDetail(@Path("characterId") characterId: Int?): Flowable<CharacterListResponse>

    @GET("/v1/public/characters/{characterId}/comics")
    fun getComics(@Path("characterId") characterId: Int?): Flowable<ComicDataWrapper>

    @GET()
    fun getCollection(@Url url: String?): Flowable<ComicDataWrapper>

    @GET()
    fun getUrl(@Url url: String?): Flowable<ComicDataWrapper>

}