package com.tiagomissiato.xpmarvel.core

import dagger.android.support.DaggerFragment

abstract class AppFragment : DaggerFragment()