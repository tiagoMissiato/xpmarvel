package com.tiagomissiato.xpmarvel.core.mvp

import android.content.Context
import io.reactivex.disposables.Disposable

interface BaseContract {

    interface View {
        fun getContext(): Context?
    }

    interface UseCase {
        fun addDisposable(disposable: Disposable)
    }
}