package com.tiagomissiato.xpmarvel.core

import io.reactivex.subjects.PublishSubject

class Communication {

    private val publishCharacterImage: PublishSubject<String> = PublishSubject.create()
    private val publishSearch: PublishSubject<String> = PublishSubject.create()
    private val publishCharacterClicked: PublishSubject<Int> = PublishSubject.create()

    fun getCharacterObserver() = publishCharacterImage

    fun getSearchObserver() = publishSearch

    fun getCharacterClickedObserver() = publishCharacterClicked

    fun postCharacterImage(image: String) = publishCharacterImage.onNext(image)

    fun postSearchTerm(term: String) = publishSearch.onNext(term)

    fun postCharacterClicked(id: Int) = publishCharacterClicked.onNext(id)
}