package com.tiagomissiato.xpmarvel.core.behaviour

import com.tiagomissiato.xpmarvel.core.mvp.ErrorStateView
import com.tiagomissiato.xpmarvel.extention.fire
import io.reactivex.Flowable
import io.reactivex.FlowableTransformer
import io.reactivex.Scheduler
import org.reactivestreams.Publisher

class ErrorStateBehaviour<T>(val view: ErrorStateView, private val uiScheduler: Scheduler) : FlowableTransformer<T, T> {

    override fun apply(upstream: Flowable<T>): Publisher<T> {

        return upstream.doOnSubscribe {
            view.hideErrorState().fire(uiScheduler)
        }
    }
}