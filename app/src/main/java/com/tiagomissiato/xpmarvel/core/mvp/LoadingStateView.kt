package com.tiagomissiato.xpmarvel.core.mvp

import io.reactivex.functions.Action

interface LoadingStateView {
    fun showLoading(): Action
    fun hideLoading(): Action
}