package com.tiagomissiato.xpmarvel.core.mvp

import com.tiagomissiato.xpmarvel.core.lifecycle.LifecycleStrategist
import io.reactivex.disposables.Disposable

open class BasePresenter<out V: BaseContract.View>(
        protected val view: V,
        private val strategist: LifecycleStrategist
) : BaseContract.UseCase {

    override fun addDisposable(disposable: Disposable) {
        strategist.applyStrategy(disposable)
    }

}