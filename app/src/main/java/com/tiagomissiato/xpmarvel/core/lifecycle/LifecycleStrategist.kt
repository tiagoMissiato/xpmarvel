package com.tiagomissiato.xpmarvel.core.lifecycle


import android.arch.lifecycle.LifecycleOwner
import io.reactivex.disposables.Disposable

open class LifecycleStrategist(owner: LifecycleOwner, private val strategy: DisposeStrategy?) {

    init {
        strategy?.let { owner.lifecycle.addObserver(it) }
    }

    fun applyStrategy(toDispose: Disposable?) {
        strategy?.let {
            toDispose?.let { strategy.addDisposable(it) }
        }
    }

}