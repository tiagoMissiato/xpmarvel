package com.tiagomissiato.xpmarvel.core.behaviour

import com.tiagomissiato.xpmarvel.core.mvp.LoadingStateView
import com.tiagomissiato.xpmarvel.extention.fire
import io.reactivex.Flowable
import io.reactivex.FlowableTransformer
import io.reactivex.Scheduler
import org.reactivestreams.Publisher

class LoadingStateBehaviour<T>(val view: LoadingStateView,
                               private val uiScheduler: Scheduler) : FlowableTransformer<T, T> {

    override fun apply(upstream: Flowable<T>): Publisher<T> {
        return upstream.doOnSubscribe {
            view.showLoading().fire(uiScheduler)
        }.doOnTerminate {
            view.hideLoading().fire(uiScheduler)
        }
    }
}