package com.tiagomissiato.xpmarvel.extention

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.tiagomissiato.xpmarvel.helper.PreferenceHelper
import com.tiagomissiato.xpmarvel.model.Character
import com.tiagomissiato.xpmarvel.model.CharacterDataContainer
import com.tiagomissiato.xpmarvel.model.CharacterListResponse
import com.tiagomissiato.xpmarvel.model.Item
import io.reactivex.Flowable

/**
 * Created by tiagomissiato on 6/5/18.
 */
fun Character.toJson(): String = Gson().toJson(this)
fun Character.saveFavorite(prefs: PreferenceHelper) = prefs.addRemoveCharacter(this)
fun <T> MutableList<T>.toJson() = Gson().toJson(this)
fun String.toCharacterList(): MutableList<Character> = if (this.isEmpty()) {
        mutableListOf()
    } else {
        Gson().fromJson<MutableList<Character>>(this, object : TypeToken<MutableList<Character>>() {}.type)
    }

fun List<Character>.toResponseFlowable(): Flowable<CharacterListResponse>
        = Flowable.just(CharacterListResponse(data = CharacterDataContainer(results = this.toList(), offset = 0)))

fun Character.toListResponse(): Flowable<CharacterListResponse>
        =  Flowable.just(CharacterListResponse(data = CharacterDataContainer(results = arrayListOf(this), offset = 0)))

fun List<Item>.toPairList() = this.flatMap { kotlin.collections.listOf(kotlin.Pair(it.name, it.resourceURI)) }