package com.tiagomissiato.xpmarvel.extention

import io.reactivex.Completable
import io.reactivex.Scheduler
import io.reactivex.functions.Action

fun Action.fire(scheduler: Scheduler) {
    Completable.fromAction(this)
            .subscribeOn(scheduler)
            .subscribe()
}