package com.tiagomissiato.xpmarvel.extention

import android.content.res.Resources
import android.view.View
import com.tiagomissiato.xpmarvel.R

/**
 * Created by tiagomissiato on 6/7/18.
 */
fun Resources.hasTwoPanes() = this.getBoolean(R.bool.has_two_panes)

fun View.hide() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}