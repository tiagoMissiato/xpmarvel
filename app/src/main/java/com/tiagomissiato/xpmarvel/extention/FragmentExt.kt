package com.tiagomissiato.xpmarvel.extention

import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity

fun AppCompatActivity.fragmentManager(block: FragmentTransaction.() -> FragmentTransaction) {
    this.supportFragmentManager
            .beginTransaction()
            .block()
            .commitAllowingStateLoss()
}