package com.tiagomissiato.xpmarvel.di.module

import android.arch.lifecycle.LifecycleOwner
import com.tiagomissiato.xpmarvel.core.lifecycle.DisposeStrategy
import com.tiagomissiato.xpmarvel.core.lifecycle.LifecycleStrategist
import dagger.Module
import dagger.Provides

@Module
class LifecycleStrategistModule {

    @Provides
    fun provideStrategist(owner: LifecycleOwner): LifecycleStrategist
            = LifecycleStrategist(owner, DisposeStrategy())

}