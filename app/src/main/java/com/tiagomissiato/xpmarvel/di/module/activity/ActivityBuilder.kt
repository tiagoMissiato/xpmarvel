package com.tiagomissiato.xpmarvel.di.module.activity

import com.tiagomissiato.xpmarvel.MainActivity
import com.tiagomissiato.xpmarvel.characterdetail.CharacterDetailActivity
import com.tiagomissiato.xpmarvel.characterlist.CharacterListFragment
import com.tiagomissiato.xpmarvel.di.module.CharacterDetailActivityModule
import com.tiagomissiato.xpmarvel.di.module.CharacterListModule
import com.tiagomissiato.xpmarvel.di.module.MainActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
@Suppress("unused")
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [(MainActivityModule::class)])
    internal abstract fun contributeMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [(CharacterDetailActivityModule::class)])
    internal abstract fun contributeCharacterDetailActivity(): CharacterDetailActivity

}