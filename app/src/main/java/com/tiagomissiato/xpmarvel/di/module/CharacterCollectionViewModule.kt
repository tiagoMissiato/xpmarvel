package com.tiagomissiato.xpmarvel.di.module

import com.tiagomissiato.xpmarvel.charactercollection.CharacterCollectionContract
import com.tiagomissiato.xpmarvel.charactercollection.CharacterCollectionView
import com.tiagomissiato.xpmarvel.core.mvp.ErrorStateView
import com.tiagomissiato.xpmarvel.core.mvp.LoadingStateView
import dagger.Binds
import dagger.Module

/**
 * Created by tiagomissiato on 6/4/18.
 */
@Module(
        includes = [
        (LifecycleStrategistModule::class),
        (PresenterModule::class)
        ]
)
@Suppress("unused")
abstract class CharacterCollectionViewModule {

    @Binds
    abstract fun characterCharacterCollectionListView(view: CharacterCollectionView): CharacterCollectionContract.View

    @Binds
    abstract fun characterLoadingView(view: CharacterCollectionView): LoadingStateView

    @Binds
    abstract fun characterErrorView(view: CharacterCollectionView): ErrorStateView

}