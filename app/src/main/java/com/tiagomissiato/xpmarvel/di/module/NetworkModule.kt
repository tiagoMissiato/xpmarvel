package com.tiagomissiato.xpmarvel.di.module

import android.content.Context
import com.tiagomissiato.xpmarvel.BuildConfig
import com.tiagomissiato.xpmarvel.R
import com.tiagomissiato.xpmarvel.core.MarvelApi
import com.tiagomissiato.xpmarvel.extention.md5
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

@Module(includes = [(AppModule::class)])
@Suppress("unused")
open class NetworkModule {

    @Provides
    @Reusable
    @Named("base_url")
    fun provideBaseUrl(context: Context): String = context.getString(R.string.base_url)

    @Provides
    @Reusable
    fun providerLogger(): Interceptor {
        val logger = HttpLoggingInterceptor()
        logger.level = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE

        return logger
    }

    @Provides
    @Reusable
    @Named("apiInterceptor")
    fun providerApiInterceptor(): Interceptor = Interceptor {
            val originalRequest = it.request()
            val ts = System.currentTimeMillis()

            var newRequestUrl
                    = originalRequest.url()
                    .newBuilder()
                    .addQueryParameter("apikey", "02989c6caf2029a4791bbd64db9ad226")
                    .addQueryParameter("hash", "${ts}2d47465e6333be2a5cb588493043823a60ff904b02989c6caf2029a4791bbd64db9ad226".md5())
                    .addQueryParameter("ts", ts.toString())
                    .build()

            // Request customization: add request headers
            val requestBuilder = originalRequest.newBuilder()
                    .url(newRequestUrl)

            val request = requestBuilder.build()
            it.proceed(request)
        }


    @Provides
    @Reusable
    fun provideGsonConverter(): GsonConverterFactory = GsonConverterFactory.create()

    @Provides
    @Reusable
    fun provideOkHttp(@Named("apiInterceptor") apiInterceptor: Interceptor,
                      logger: Interceptor): OkHttpClient
            = OkHttpClient.Builder()
            .addInterceptor(apiInterceptor)
            .addInterceptor(logger).build()

    @Provides
    @Reusable
    fun provideAdapterFactory(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()

    @Provides
    @Reusable
    fun provideRetrofit(@Named("base_url") baseUrl: String,
                        okHttpClient: OkHttpClient,
                        rxAdapterFactory: RxJava2CallAdapterFactory,
                        gsonConverter: GsonConverterFactory): Retrofit
        = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(baseUrl)
            .addConverterFactory(gsonConverter)
            .addCallAdapterFactory(rxAdapterFactory)
            .build()

    @Provides
    @Reusable
    fun provideMarvelApi(retrofit: Retrofit): MarvelApi = retrofit.create(MarvelApi::class.java)
}