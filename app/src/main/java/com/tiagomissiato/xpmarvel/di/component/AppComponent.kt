package com.tiagomissiato.xpmarvel.di.component

import com.tiagomissiato.xpmarvel.core.AppApplication
import com.tiagomissiato.xpmarvel.di.module.*
import com.tiagomissiato.xpmarvel.di.module.activity.ActivityBuilder
import com.tiagomissiato.xpmarvel.di.module.fragment.FragmentBuilder
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            (AndroidSupportInjectionModule::class),
            (AppModule::class),
            (NetworkModule::class),
            (ActivityBuilder::class),
            (FragmentBuilder::class),
            (PreferenceModule::class),
            (CommunicationModule::class)
        ]
)
interface AppComponent : AndroidInjector<AppApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: AppApplication): AppComponent.Builder

        fun build(): AppComponent

    }
}