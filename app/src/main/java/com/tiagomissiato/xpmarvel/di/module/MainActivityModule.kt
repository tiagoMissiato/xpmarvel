package com.tiagomissiato.xpmarvel.di.module

import android.arch.lifecycle.LifecycleOwner
import com.tiagomissiato.xpmarvel.MainActivity
import dagger.Binds
import dagger.Module

@Module(
        includes = [
            (LifecycleStrategistModule::class),
            (PresenterModule::class)
        ]
)
abstract class MainActivityModule {

    @Binds
    abstract fun strategist(activity: MainActivity): LifecycleOwner

}