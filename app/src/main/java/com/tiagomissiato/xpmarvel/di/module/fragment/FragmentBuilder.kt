package com.tiagomissiato.xpmarvel.di.module.fragment

import com.tiagomissiato.xpmarvel.characterdetail.CharacterDetailFragment
import com.tiagomissiato.xpmarvel.characterlist.CharacterListFragment
import com.tiagomissiato.xpmarvel.di.module.CharacterDetailModule
import com.tiagomissiato.xpmarvel.di.module.CharacterListModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
@Suppress("unused")
abstract class FragmentBuilder {

    @ContributesAndroidInjector(modules = [(CharacterListModule::class)])
    internal abstract fun characterListFragment(): CharacterListFragment

    @ContributesAndroidInjector(modules = [(CharacterDetailModule::class)])
    internal abstract fun characterDetailFragment(): CharacterDetailFragment

}