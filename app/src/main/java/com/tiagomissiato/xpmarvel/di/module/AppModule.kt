package com.tiagomissiato.xpmarvel.di.module

import android.content.Context
import com.tiagomissiato.xpmarvel.core.AppApplication
import com.tiagomissiato.xpmarvel.di.qualifiers.IOScheduler
import com.tiagomissiato.xpmarvel.di.qualifiers.UIScheduler
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton


@Module
@Suppress("unused")
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: AppApplication): Context = application

    @Provides
    @Singleton
    @UIScheduler
    fun uiScheduler(): Scheduler = AndroidSchedulers.mainThread()

    @Provides
    @Singleton
    @IOScheduler
    fun ioScheduler(): Scheduler = Schedulers.io()
}