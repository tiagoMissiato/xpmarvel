package com.tiagomissiato.xpmarvel.di.module

import com.tiagomissiato.xpmarvel.core.Communication
import dagger.Module
import dagger.Provides
import dagger.Reusable
import javax.inject.Singleton

@Module
@Suppress("unused")
open class CommunicationModule {

    @Provides
    @Singleton
    fun provideCommunication() = Communication()

}