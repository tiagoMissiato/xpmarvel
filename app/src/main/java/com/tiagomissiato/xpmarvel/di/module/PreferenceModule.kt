package com.tiagomissiato.xpmarvel.di.module

import android.content.Context
import android.content.SharedPreferences
import com.tiagomissiato.xpmarvel.helper.PreferenceHelper
import dagger.Module
import dagger.Provides

@Module
@Suppress("unused")
class PreferenceModule {

    object Constant {
        val SHARED_PREFERENCE_NAME = "com.tiagomissiato.xpmarvel.SHARED_PREFERENCE_NAME"
    }

    @Provides
    fun providePreference(context: Context): SharedPreferences
            = context.getSharedPreferences(Constant.SHARED_PREFERENCE_NAME, 0)

    @Provides
    fun providePreferenceHelper(prefs: SharedPreferences): PreferenceHelper
            = PreferenceHelper(prefs)
}