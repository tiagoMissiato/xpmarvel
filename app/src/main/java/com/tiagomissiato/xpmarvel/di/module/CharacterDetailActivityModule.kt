package com.tiagomissiato.xpmarvel.di.module

import android.arch.lifecycle.LifecycleOwner
import com.tiagomissiato.xpmarvel.MainActivity
import com.tiagomissiato.xpmarvel.characterdetail.CharacterDetailActivity
import dagger.Binds
import dagger.Module

@Module(
        includes = [
            (LifecycleStrategistModule::class),
            (PresenterModule::class)
        ]
)
abstract class CharacterDetailActivityModule {

    @Binds
    abstract fun strategist(activity: CharacterDetailActivity): LifecycleOwner

}