package com.tiagomissiato.xpmarvel.di.module

import android.arch.lifecycle.LifecycleOwner
import com.tiagomissiato.xpmarvel.characterlist.CharacterListContract
import com.tiagomissiato.xpmarvel.characterlist.CharacterListFragment
import com.tiagomissiato.xpmarvel.core.mvp.ErrorStateView
import com.tiagomissiato.xpmarvel.core.mvp.LoadingStateView
import dagger.Binds
import dagger.Module

@Module(
        includes = [
            (LifecycleStrategistModule::class),
            (PresenterModule::class)
        ]
)
@Suppress("unused")
abstract class CharacterListModule {

    @Binds
    abstract fun characterListView(fragment: CharacterListFragment): CharacterListContract.View

    @Binds
    abstract fun characterLoadingView(fragment: CharacterListFragment): LoadingStateView

    @Binds
    abstract fun characterErrorView(fragment: CharacterListFragment): ErrorStateView

    @Binds
    abstract fun lifecycleOwner(fragment: CharacterListFragment): LifecycleOwner

}