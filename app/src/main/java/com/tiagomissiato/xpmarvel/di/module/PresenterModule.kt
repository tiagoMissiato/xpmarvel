package com.tiagomissiato.xpmarvel.di.module

import com.tiagomissiato.xpmarvel.characterdetail.CharacterDetailContract
import com.tiagomissiato.xpmarvel.characterdetail.CharacterDetailPresenter
import com.tiagomissiato.xpmarvel.characterdetail.CharacterDetailRepository
import com.tiagomissiato.xpmarvel.characterlist.CharacterListContract
import com.tiagomissiato.xpmarvel.characterlist.CharacterListPresenter
import com.tiagomissiato.xpmarvel.characterlist.CharacterListRepository
import com.tiagomissiato.xpmarvel.core.behaviour.Behaviours
import com.tiagomissiato.xpmarvel.core.lifecycle.LifecycleStrategist
import com.tiagomissiato.xpmarvel.di.qualifiers.IOScheduler
import com.tiagomissiato.xpmarvel.di.qualifiers.UIScheduler
import com.tiagomissiato.xpmarvel.model.Character
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler

@Module
@Suppress("unused")
class PresenterModule {

    @Provides
    fun provideCharacterListPresenter(view: CharacterListContract.View,
                                      strategist: LifecycleStrategist,
                                      repository: CharacterListRepository,
                                      @IOScheduler ioScheduler: Scheduler,
                                      @UIScheduler uiScheduler: Scheduler): CharacterListPresenter
            = CharacterListPresenter(view, strategist, repository, ioScheduler, uiScheduler)


    @Provides
    fun provideCharacterDetailPresenter(view: CharacterDetailContract.View,
                                      strategist: LifecycleStrategist,
                                      repository: CharacterDetailRepository,
                                      @IOScheduler ioScheduler: Scheduler,
                                      @UIScheduler uiScheduler: Scheduler): CharacterDetailPresenter
            = CharacterDetailPresenter(view, strategist, repository, ioScheduler, uiScheduler)
}