package com.tiagomissiato.xpmarvel.di.module

import android.arch.lifecycle.LifecycleOwner
import com.tiagomissiato.xpmarvel.characterdetail.CharacterDetailContract
import com.tiagomissiato.xpmarvel.characterdetail.CharacterDetailFragment
import com.tiagomissiato.xpmarvel.characterlist.CharacterListContract
import com.tiagomissiato.xpmarvel.characterlist.CharacterListFragment
import com.tiagomissiato.xpmarvel.core.mvp.ErrorStateView
import com.tiagomissiato.xpmarvel.core.mvp.LoadingStateView
import dagger.Binds
import dagger.Module

@Module(
        includes = [
            (LifecycleStrategistModule::class),
            (PresenterModule::class)
        ]
)
@Suppress("unused")
abstract class CharacterDetailModule {

    @Binds
    abstract fun characterListView(fragment: CharacterDetailFragment): CharacterDetailContract.View

    @Binds
    abstract fun characterLoadingView(fragment: CharacterDetailFragment): LoadingStateView

    @Binds
    abstract fun characterErrorView(fragment: CharacterDetailFragment): ErrorStateView

    @Binds
    abstract fun lifecycleOwner(fragment: CharacterDetailFragment): LifecycleOwner

}