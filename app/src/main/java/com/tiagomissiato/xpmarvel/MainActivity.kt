package com.tiagomissiato.xpmarvel

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.view.View
import android.widget.Toast
import com.jakewharton.rxbinding2.widget.RxTextView
import com.tiagomissiato.xpmarvel.characterlist.CharacterListFragment
import com.tiagomissiato.xpmarvel.characterlist.CharacterListRepository
import com.tiagomissiato.xpmarvel.core.AppActivity
import com.tiagomissiato.xpmarvel.core.AppFragment
import com.tiagomissiato.xpmarvel.core.Communication
import com.tiagomissiato.xpmarvel.core.lifecycle.LifecycleStrategist
import com.tiagomissiato.xpmarvel.extention.fragmentManager
import com.tiagomissiato.xpmarvel.extention.hasTwoPanes
import kotlinx.android.synthetic.main.activity_character_detail.*
import kotlinx.android.synthetic.main.include_toolbar_search.view.*
import javax.inject.Inject
import kotlinx.android.synthetic.main.one_pane.appbar as appBarPhone
import kotlinx.android.synthetic.main.one_pane.navigation as navigationPhone
import kotlinx.android.synthetic.main.two_panes.appbar as appBarTablet
import kotlinx.android.synthetic.main.two_panes.navigation as navigationTablet

class MainActivity : AppActivity(){

    @Inject
    lateinit var strategist: LifecycleStrategist
    @Inject
    lateinit var communication: Communication

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        var fragment: AppFragment? = when (item.itemId) {
            R.id.navigation_home -> {
                setTabSelected(R.id.navigation_home)
                CharacterListFragment.newInstance()
            }
            R.id.navigation_dashboard -> {
                setTabSelected(R.id.navigation_dashboard)
                CharacterListFragment.newInstance(CharacterListRepository.Source.LOCAL)
            }
            else -> {
                Toast.makeText(baseContext, "Teste para Desenvolvedor Android Senior - XP Investimentos", Toast.LENGTH_LONG).show()
                null
            }
        }

        fragment?.let {
            fragmentManager {
                replace(if (resources.hasTwoPanes()) R.id.listContainer else R.id.content, fragment)
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true)
            actionBar.title = null
        }

        val disposable = if(resources.hasTwoPanes()){
            appBarTablet.clearIcon.setOnClickListener { appBarTablet.searchText.text.clear() }
            navigationTablet.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
            RxTextView.textChanges(appBarTablet.searchText).subscribe {
                if (it.toString().isEmpty()) {
                    appBarTablet.clearIcon.visibility = View.GONE
                } else {
                    appBarTablet.clearIcon.visibility = View.VISIBLE
                }
                communication.postSearchTerm(it.toString())
            }
        } else {
            appBarPhone.clearIcon.setOnClickListener { appBarPhone.searchText.text.clear() }
            navigationPhone.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
            fragmentManager {
                add(R.id.content, CharacterListFragment.newInstance())
            }
            RxTextView.textChanges(appBarPhone.searchText).subscribe {
                if (it.toString().isEmpty()) {
                    appBarPhone.clearIcon.visibility = View.GONE
                } else {
                    appBarPhone.clearIcon.visibility = View.VISIBLE
                }
                communication.postSearchTerm(it.toString())
            }
        }

        strategist.applyStrategy(disposable)
    }

    private fun setTabSelected(id: Int) {
        if (resources.hasTwoPanes()) {
            navigationTablet.menu.findItem(id).isChecked = true
        } else {
            navigationPhone.menu.findItem(id).isChecked = true
        }
    }
}
